using ChessBackend.Logic;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ChessBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IChessService, ChessService>();
            services.AddSingleton<IStateService, StateService>();
            
            services.AddCors(options =>
            {
                options.AddPolicy("MyOrigin",
                    builder => builder.AllowAnyOrigin() 
                        .AllowAnyHeader()
                        .AllowAnyMethod()); 
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors("MyOrigin");
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
