﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="ZigZag">
//     Copyright © ZigZag 2023
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ChessBackend.Logic
{
    public static class Constants
    {
        public static int[,] BashniaDirections = new int[,] { {-1, 0}, {1, 0}, {0, 1}, {0, -1} };
        public static int[,] SlonDirections = new int[,] { {-1, -1}, {1, 1}, {-1, 1}, {1, -1} };
        public static int[,] QueenDirections = new int[,] 
            {{-1, 0}, {1, 0}, {0, 1}, {0, -1}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};      
        public static int[,] KingDirections = new int[,] 
            {{-1, 0}, {1, 0}, {0, 1}, {0, -1}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
        public static int[,] HorseDirections = new int[,] 
            {{1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2}};
    }
}
