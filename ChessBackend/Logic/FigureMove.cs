﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OldPosition.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ChessBackend.Logic
{
    public class FigureMove
    {
        public FigurePosition OldPosition {get; set;}
        
        public Position NewPosition {get; set;}
        
        public bool isEat {get; set;}
    }
}
