﻿

using System.Collections.Generic;

namespace ChessBackend.Logic.Properties
{
    public static class Move
    {
        public static void MakeMove(string[][] bord, char color)
        {

            var currentColorFigures = Hellper.GetFigures(bord, color);
            
            var currentColorEat = Eat(currentColorFigures, bord, color);
            var currentColorStep = Step(currentColorFigures, bord, color);
            
            var enemycolor = ' ';

            if (color == 'ч')
            {
                enemycolor = 'б';
            }
            else
            {
                enemycolor = 'ч';
            }
            
            var enemyColorFigures = Hellper.GetFigures(bord, enemycolor);
            var enemycoloreat = Eat(enemyColorFigures, bord, enemycolor);
            
            var isMoveMade = false;
            isMoveMade = MoveUnderAttak(bord, "К", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            
            if(isMoveMade == false)
            {
                isMoveMade = MoveUnderAttak(bord, "Ф", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            }
            if(isMoveMade == false)
            {
                isMoveMade = MoveUnderAttak(bord, "Б", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            }
            if(isMoveMade == false)
            {
                isMoveMade = MoveUnderAttak(bord, "С", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            }
            if(isMoveMade == false)
            {
                isMoveMade = MoveUnderAttak(bord, "Л", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            }
            if(isMoveMade == false)
            {
                isMoveMade = MoveUnderAttak(bord, "П", currentColorEat, currentColorStep, enemycoloreat, currentColorFigures);
            }
            
            if(isMoveMade == false)
            {
                if(currentColorEat.Count > 0)
                {
                    MakeRandomMove(bord, currentColorEat);
                }
                else
                {
                    MakeRandomMove(bord, currentColorStep);
                }
            }

            Hellper.ShowBord(bord);
        }
        
        public static List<FigureMove> Eat(List<FigurePosition> figures, string[][] bord, char color)
        {
            var result = new List<FigureMove>();
            foreach (var figure in figures)
            {
                if (figure.Name.Contains(color + "П"))
                {
                    result.AddRange(StepPeshka.Eat(figure, bord));
                }
                
                if (figure.Name.Contains(color + "Ф"))
                {
                    result.AddRange(StepQueen.Eat(figure, bord));
                }
                if (figure.Name.Contains(color + "С"))
                {
                    result.AddRange(StepSlon.Eat(figure, bord));
                    
                }
                if (figure.Name.Contains(color + "К"))
                {
                    result.AddRange(StepKing.Eat(figure, bord));
                }
                if (figure.Name.Contains(color + "Л"))
                {
                    result.AddRange(StepHorse.Eat(figure, bord));
                }
                if (figure.Name.Contains(color + "Б"))
                {
                    result.AddRange(StepBashna.Eat(figure, bord));
                }
                
            }
            
            return result;
            
        }
        
        public static List<FigureMove> Step(List<FigurePosition> figures, string[][] bord, char color)
        {
            var result = new List<FigureMove>();
            foreach (var figure in figures)
            {
                if (figure.Name.Contains(color + "П"))
                {
                    result.AddRange(StepPeshka.Step(figure, bord));
                }
                
                if (figure.Name.Contains(color + "Ф"))
                {
                    result.AddRange(StepQueen.Step(figure, bord));
                }
                if (figure.Name.Contains(color + "С"))
                {
                    result.AddRange(StepSlon.Step(figure, bord));
                    
                }
                if (figure.Name.Contains(color + "К"))
                {
                    result.AddRange(StepKing.Step(figure, bord));
                }
                if (figure.Name.Contains(color + "Л"))
                {
                    result.AddRange(StepHorse.Step(figure, bord));
                }
                if (figure.Name.Contains(color + "Б"))
                {
                    result.AddRange(StepBashna.Step(figure, bord));
                }
                
            }
            
            return result;
        }

        public static bool UnderAttack(FigurePosition pos, List<FigureMove> enemycoloreat)
        {
            var result = false;

            foreach (var i in enemycoloreat)
            {
                if(i.NewPosition.X == pos.X && i.NewPosition.Y == pos.Y)
                {
                    result = true;

                    return result;
                }
            }
            
            return result;
        }
        
        public static List<FigurePosition> GetPositionsByFigure(List<FigurePosition> figures, string figureType)
        {
            var result = new List<FigurePosition>();

            foreach (var figure in figures)
            {
                if (figure.Name.Contains(figureType))
                {
                    result.Add(figure);
                }
            }
            return result;
        }
        
        public static bool MoveUnderAttak(
            string[][] bord,
            string figureType,
            List<FigureMove> currentColorEat,
            List<FigureMove> currentColorStep,
            List<FigureMove> enemycoloreat,
            List<FigurePosition> figures)
        {
            var positions = GetPositionsByFigure(figures, figureType);

            foreach (var position in positions)
            {
                if(UnderAttack(position, enemycoloreat))
                {
                    var currentPositionEats = getCurrentPositionActions(position, currentColorEat);
                    
                    if(currentPositionEats.Count > 0)
                    {
                        MakeRandomMove(bord, currentPositionEats);
                        return true;
                    }
                    
                    
                    var currentPositionSteps = getCurrentPositionActions(position, currentColorStep);

                    if (currentPositionSteps.Count > 0)
                    {
                        MakeRandomMove(bord, currentPositionSteps);
                        return true;
                    }
                    
                }
            }
            
            return false;
        }
        
        public static List<FigureMove> getCurrentTypeActions(string figureType, List<FigureMove> moves)
        {
            var result = new List<FigureMove>();

            foreach (var move in moves)
            {
                if (move.OldPosition.Name.Contains(figureType))
                {
                    result.Add(move);
                }
            }
            
            return result;
        }
        
        public static List<FigureMove> getCurrentPositionActions(Position position, List<FigureMove> moves)
        {
            var result = new List<FigureMove>();

            foreach (var move in moves)
            {
                if (move.OldPosition.X == position.X && move.OldPosition.Y == position.Y)
                {
                    result.Add(move);
                }
            }
            
            return result;
        }


        public static void MakeRandomMove(string[][] bord, List<FigureMove> moves)
        {
            var i = MyRandom.Get(0, moves.Count);
            
            var pOld = moves[i].OldPosition;
            var pNew = moves[i].NewPosition;
            
            Hellper.MakeMove(bord, pOld, pNew);
        }
    }
    
    
    
}
