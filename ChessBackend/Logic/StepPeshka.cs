﻿
using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class StepPeshka
    {
        public static List<FigureMove> Step(FigurePosition pOld, string[][] bord)
        {
            var result = new List<FigureMove>();
            
            var np = new Position();
            np.X = pOld.X;
            
            if (pOld.Name[0] == 'б')
            {
                np.Y = pOld.Y + 1;
            }
            else
            {
                np.Y = pOld.Y - 1;
            }

            var isFirstCellFree = Hellper.IsFree(np, bord);
            
            if (isFirstCellFree)
            {
                var fm = new FigureMove();
                fm.isEat = false;
                fm.NewPosition = np;
                fm.OldPosition = pOld;
                result.Add(fm);
            }
            
            /////////////////////////////////////////////////
            
            if(pOld.Y != 1 && pOld.Name[0] == 'б')
            {
                return result;
            }
            
            if(pOld.Y != 6 && pOld.Name[0] == 'ч')
            {
                return result;
            }

            //////////////////////////////////
            np = new Position();
            np.X = pOld.X;
            
            if (pOld.Name[0] == 'б')
            {
                np.Y = pOld.Y + 2;
            }
            else
            {
                np.Y = pOld.Y - 2;
            }

            if (Hellper.IsFree(np, bord) && isFirstCellFree)  
            {
                var fm = new FigureMove();
                fm.isEat = false;
                fm.NewPosition = np;
                fm.OldPosition = pOld;
                result.Add(fm);
            }
            
            return result;
        }

        public static List<FigureMove> Eat(FigurePosition pOld, string[][] bord)
        {
            var eatCoordinates = GetEatCoordinates(pOld, bord);
            var result = Shared.GetFigureMoves(eatCoordinates, pOld);

             return result;
        }
        
        private static List<Position> GetEatCoordinates(FigurePosition pOld, string[][] bord)
        {
            var r = new List<Position>();
            
            var pNew = new Position();
            if (pOld.Name[0] == 'б')
            {
                pNew.X = pOld.X - 1;
                pNew.Y = pOld.Y + 1;
            }
            else
            {
                pNew.X = pOld.X - 1;
                pNew.Y = pOld.Y - 1;
            }
            
            if (Hellper.IsInsideBoard(pNew) && Hellper.IsEnemy(pOld, pNew, bord))
            {
                 r.Add(pNew);
            }
            
            pNew = new Position();
            if (pOld.Name[0] == 'б')
            {
                pNew.X = pOld.X + 1;
                pNew.Y = pOld.Y + 1;
            }
            if (pOld.Name[0] == 'ч')
            {
                pNew.X = pOld.X + 1;
                pNew.Y = pOld.Y - 1;
            }
            
            if (Hellper.IsInsideBoard(pNew) && Hellper.IsEnemy(pOld, pNew, bord))
            {
                r.Add(pNew);
            }
            
            return r;
        }
        
    }
}