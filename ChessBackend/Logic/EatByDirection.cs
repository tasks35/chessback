﻿using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class EatByDirection
    {
        public static List<FigureMove> Eat(FigurePosition pOld, string[][] bord, int[,] directions, int maxDistance)
        {
            var eatCoordinates = GetEatCoordinates(pOld, bord, directions, maxDistance);
            var result = Shared.GetFigureMoves(eatCoordinates, pOld);
            
            return result;
        }
        
        private static List<Position> GetEatCoordinates(FigurePosition pOld, string[][] bord, int[,] directions, int maxDistance)
        {
            var r = new List<Position>();

            for (int i = 0; i < directions.GetLength(0) ; i++)
            {
                var xOffset = directions[i,0];
                var yOffset = directions[i,1];
                var position = CheckEatCoordinatesByDirection(yOffset, xOffset, pOld, bord, maxDistance);

                if (position != null)
                {
                    r.Add(position);
                }
            } 
           
            return r;
        }
        
        private static Position CheckEatCoordinatesByDirection(
            int yOffset, int xOffset, FigurePosition pOld, string[][] bord, int maxDistance)
        {
            var counter = 0;
            
            var pNew = new Position();
            pNew.X = pOld.X + xOffset;
            pNew.Y = pOld.Y + yOffset;

            while (Hellper.IsInsideBoard(pNew) && counter < maxDistance)
            {
                if (!Hellper.IsFree(pNew, bord))
                {
                    if (Hellper.IsEnemy(pOld, pNew, bord))
                    {
                        return pNew;
                    }
                    
                    return null;
                    
                }
                
                pNew.X = pNew.X + xOffset;
                pNew.Y = pNew.Y + yOffset;
                counter++;
            }
            
            return null;
        }
    }
}