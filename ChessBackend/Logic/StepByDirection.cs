﻿using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class StepByDirection
    {
        public static List<FigureMove> Step(FigurePosition pOld, string[][] bord, int[,] directions, int maxDistance)
        {
            var result = new List<FigureMove>();
            var stepCoordinates = GetStepCoordinates(pOld, bord, directions, maxDistance);

            foreach (var stepCoordinate in stepCoordinates)
            {
                var j = new FigureMove();
                j.isEat = false;
                j.NewPosition = stepCoordinate;
                j.OldPosition = pOld;
                result.Add(j);
            }
            
            return result;
        }
        
        private static List<Position> GetStepCoordinates(FigurePosition pOld, string[][] bord, int[,] directions, int maxDistance)
        {
            var r = new List<Position>();
            
            
            
            for (int i = 0; i < directions.GetLength(0); i++)
            {
                var xOffset = directions[i,0];
                var yOffset = directions[i,1];
                var positions = GetStepCoordinatesByDirection(yOffset, xOffset, pOld, bord, maxDistance);

                r.AddRange(positions);
            } 
            
            return r;
        }
        
        private static List<Position> GetStepCoordinatesByDirection(
            int yOffset, int xOffset, FigurePosition pOld, string[][] bord, int maxDistance)
        {
            var r = new List<Position>();
            
            var counter = 0;
            
            var pNew = new Position();
            pNew.X = pOld.X + xOffset;
            pNew.Y = pOld.Y + yOffset;

            while (Hellper.IsInsideBoard(pNew) && counter < maxDistance)
            {
                if (Hellper.IsFree(pNew, bord))
                {
                    var coords = new Position(); 
                    coords.X = pNew.X;
                    coords.Y = pNew.Y;
                    r.Add(coords);
                }
                
                if (!Hellper.IsFree(pNew, bord))
                {
                    return r;
                }
                
                pNew.X = pNew.X + xOffset;
                pNew.Y = pNew.Y + yOffset;
                counter++;
            }
            
            return r;
        }
    }
}