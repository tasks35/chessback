﻿
namespace ChessBackend.Logic
{
    public class StateService : IStateService
    {
        public string Turn { get; set; }
        public string[][] Bord { get; set; }
    }
}
