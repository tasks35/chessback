﻿
namespace ChessBackend.Logic
{
    public interface IStateService
    {
        string Turn {get; set;}
        
        string[][] Bord {get; set;}
    }
}
