﻿namespace ChessBackend.Logic
{
    public interface IChessService
    {
        string[][] MakeAllSteps();
        
        string[][] MakeStep();
        
        string[][] MakeManualStep(FigureMove move);
    }
}
