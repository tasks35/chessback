﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FigurePosition.cs" company="ZigZag">
//     Copyright © ZigZag 2023
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ChessBackend.Logic
{
    public class FigurePosition : Position
    {
       public string Name {get; set;}
    }
}