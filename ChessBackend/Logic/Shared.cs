﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Shared.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class Shared
    {
        public static List<FigureMove> GetFigureMoves(List<Position> collection, FigurePosition pOld)
        {
            var result = new List<FigureMove>();
            
            foreach (var eatCoordinate in collection)
            {
                var figureMove = new FigureMove();
                figureMove.OldPosition = pOld;
                figureMove.NewPosition = eatCoordinate;
                figureMove.isEat = true;
                result.Add(figureMove);
            }
            return result;
        }

    }
}
