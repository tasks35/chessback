﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FigurePosition.cs" company="ZigZag">
//     Copyright © ZigZag 2023
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ChessBackend.Logic
{
    public class Position
    {
       public int X {get; set;}
       public int Y {get; set;}
       
    }
}