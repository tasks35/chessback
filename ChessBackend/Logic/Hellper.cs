﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Hellper.cs" company="ZigZag">
//     Copyright © ZigZag 2023
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public class Hellper
    {
        public static string[][] GetBord()
        {
            string[] line1 = new string[] {"Б", "Л", "С", "Ф", "К", "С", "Л", "Б"};
            string[] line2 = new string[] {"П", "П", "П", "П", "П", "П", "П", "П"};
            string[] line3 = new string[] {  "..",   "..",   "..",   "..",   "..",   "..",   "..",   ".."};
            string[] line4 = new string[] {  "..",   "..",   "..",   "..",   "..",   "..",   "..",   ".."};
            string[] line5 = new string[] {  "..",   "..",   "..",   "..",   "..",   "..",   "..",   ".."};
            string[] line6 = new string[] {  "..",   "..",   "..",   "..",   "..",   "..",   "..",   ".."};
            string[] line7 = new string[] {"П", "П", "П", "П", "П", "П", "П", "П"};
            string[] line8 = new string[] {"Б", "Л", "С", "Ф", "К", "С", "Л", "Б"};
            
            string[][] Bord = { line8, line7, line6, line5, line4, line3, line2, line1 };
            
            for (int i = 0; i <= 1; i++)
            {
                var line = Bord[i];
                for (int j = 0; j <= 7; j++)
                {
                    line[j] = "б" + line[j];
                }
            }
            for (int i = 6; i <= 7; i++)
            {
                var line = Bord[i];
                for (int j = 0; j <= 7; j++)
                {
                    line[j] = "ч" + line[j];
                }
            }
            
            return Bord;
            
        }
        
        public static void ShowBord(string[][] bord)
        {
            
            for (int i = 7; i >= 0; i--)
            {
                var line = bord[i];
                

                for (int j = 0; j <= 7; j++)
                {
                    
                    var cell = line[j];
                    
                    Console.Write(cell);
                    
                }
                Console.WriteLine();
            }
        }
        
        public static List<FigurePosition> GetFigures(string[][] bord, char color)
        {
            var figures = new List<FigurePosition>();
            
            for (int y = 0; y <= 7; y++)
            {
                for (int x = 0; x <= 7; x++)
                {
                    var sell = bord[y][x];

                    if (sell.Contains(color+""))
                    {
                        var fi = new FigurePosition(); 
                        fi.X = x;
                        fi.Y = y;
                        fi.Name = sell;
                        figures.Add(fi);
                    }
                }
            }
            return figures;
            
            
        }
        public static void MakeMove(string[][] bord, Position pOld, Position pNew)
        {
            var t = bord[pOld.Y][pOld.X];
            bord[pOld.Y][pOld.X] = "..";
            bord[pNew.Y][pNew.X] = t;
        }
        
        public static bool IsFree(Position p, string[][] bord)
        {

            var r = IsInsideBoard(p) && bord[p.Y][p.X] == "..";
            
            return r;
        }

        public static bool IsInsideBoard(Position p)
        {
            return p.X >= 0 && p.X <= 7 && p.Y >= 0 && p.Y <= 7;
        }
        
        public static bool IsEnemy(Position pOld, Position pNew, string[][] bord)
        {
            
            // бП чП 
            var cellOld = bord[pOld.Y][pOld.X]; //Shared GetFigureMoves
            
            var cellNew = bord[pNew.Y][pNew.X];

            if (cellOld[0] == cellNew[0])
            {
                return false;
            }

            if (cellNew == "..")
            {
                return false;
            }

            return true;
        }
    }
}