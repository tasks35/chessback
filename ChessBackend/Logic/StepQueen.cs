﻿
using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class StepQueen
    {
        public static List<FigureMove> Eat(FigurePosition pOld, string[][] bord)
        {
            var r = EatByDirection.Eat(pOld, bord, Constants.QueenDirections, 7);
            
            return r;
        }
        public static List<FigureMove> Step(FigurePosition pOld, string[][] bord)
        {
            var r = StepByDirection.Step(pOld, bord, Constants.QueenDirections, 7);
            
            return r;
        }
    }
}