﻿using System;

using ChessBackend.Logic.Properties;

namespace ChessBackend.Logic
{
    public class ChessService : IChessService
    {
        public IStateService stateService;
        public ChessService(IStateService stateService)
        {
            this.stateService = stateService;
        }
        
        public string[][] MakeStep()
        {
            if (string.IsNullOrEmpty(stateService.Turn))
            {
                stateService.Turn = "б";
                stateService.Bord = Hellper.GetBord();
                Hellper.ShowBord(stateService.Bord);
                Console.WriteLine();
            }
            else if (stateService.Turn == "б")
            {
                stateService.Turn = "ч";
            }
            else
            {
                stateService.Turn = "б";
            }

            var colorWrite = stateService.Turn == "б" ? "БЕЛЫХ" : "ЧЕРНЫХ";
            Console.WriteLine($"------------------ Начало хода {colorWrite} -------------");
            Move.MakeMove(stateService.Bord, stateService.Turn.ToCharArray()[0]);
            Console.WriteLine($"================== Конец хода {colorWrite} =============");
            Console.WriteLine();
               
            return stateService.Bord;
        }

        public string[][] MakeManualStep(FigureMove move)
        {
            Hellper.MakeMove(stateService.Bord, move.OldPosition, move.NewPosition);
            
            var cellNew = stateService.Bord[move.NewPosition.Y][move.NewPosition.X];
            
            // for next turn
            stateService.Turn = cellNew[0].ToString();

            return stateService.Bord;
        }

        public string[][] MakeAllSteps()
        {
            var bord = Hellper.GetBord();
            Hellper.ShowBord(bord);
            Console.WriteLine();

           for (int i = 0; i < 10; i++)
           {
               Console.WriteLine("------------------ Начало хода БЕЛЫХ -------------");
               Move.MakeMove(bord, 'б');
               Console.WriteLine("================== Конец хода БЕЛЫХ =============");
               Console.WriteLine();
               
               Console.WriteLine("------------------ Начало хода ЧЕРНЫХ -------------");
               Move.MakeMove(bord, 'ч');
               Console.WriteLine("================== Конец хода ЧЕРНЫХ =============");
               Console.WriteLine();
           }
           
           return bord;
        }
    }
}