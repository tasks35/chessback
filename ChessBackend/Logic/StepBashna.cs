﻿
using System.Collections.Generic;

namespace ChessBackend.Logic
{
    public static class StepBashna
    {
        public static List<FigureMove> Eat(FigurePosition pOld, string[][] bord)
        {
            var r = EatByDirection.Eat(pOld, bord, Constants.BashniaDirections, 7);
            
            return r;
        }
        
        public static List<FigureMove> Step(FigurePosition pOld, string[][] bord)
        {
            var r = StepByDirection.Step(pOld, bord, Constants.BashniaDirections, 7);
            
            return r;
        }
    }
}