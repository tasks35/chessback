﻿using ChessBackend.Logic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ChessBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChessController : ControllerBase
    {
        private readonly ILogger<ChessController> _logger;
        private readonly IChessService chessService;

        public ChessController(
            ILogger<ChessController> logger,
            IChessService chessService)
        {
            _logger = logger;
            this.chessService = chessService;
        }

        [HttpGet]
        [Route("MakeAllSteps")]
        public string[][] MakeAllSteps()
        {
            var r = this.chessService.MakeAllSteps();
            
            return r;
        }
        
        [HttpGet]
        [Route("MakeStep")]
        public string[][] MakeStep()
        {
            var r = this.chessService.MakeStep();
            
            return r;
        }
        
        [HttpPost]
        [Route("MakeManualStep")]
        public string[][] MakeManualStep(FigureMove move)
        {
            var r = this.chessService.MakeManualStep(move);
            
            return r;
        }
    }
}
